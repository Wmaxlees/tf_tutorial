#!/usr/bin/python3

import numpy as np
import tensorflow as tf

from shared import data

# Get the data we will use for the tutorial
data, labels, test_data, test_labels = data.get_batches()

# HYPERPARAMETERS DEFINED HERE
number_of_epoches = 100

number_of_classes = 10

hidden_layer_size = 300

learning_rate = 1e-6

# Default Graph
with tf.Graph().as_default():

    # Placeholder for our inputs
    x = tf.placeholder(tf.float32, (None, 32, 32, 3, ), name='x')
    y = tf.placeholder(tf.uint8, (None, ), name='y')

    x_flat = tf.reshape(x, (-1, 3072, ))
    hidden_layer = tf.contrib.layers.fully_connected(x_flat, hidden_layer_size)
    logits = tf.contrib.layers.fully_connected(hidden_layer, number_of_classes)

    y_one_hot = tf.one_hot(y, number_of_classes)
    xent = tf.losses.softmax_cross_entropy(y_one_hot, logits)

    train_op = tf.train.AdagradOptimizer(learning_rate).minimize(xent)

    # Initialize the session
    with tf.Session() as sess:

        # Initialize all the variables
        sess.run(tf.global_variables_initializer())

        # Step through each epoch
        for epoch in range(number_of_epoches):
            # Step through each batch
            for batch_idx in range(len(data)):
                batch_data = data[batch_idx]
                batch_labels = labels[batch_idx]

                _, xent_loss = sess.run([train_op, xent], {x: batch_data, y: batch_labels})

                print('XENT: %s' % (xent_loss, ))
