import pickle
import numpy as np

def _unpickle (file):
    with open(file, 'rb') as fo:
        dictionary = pickle.load(fo, encoding='bytes')
    return dictionary

def get_batches ():
    data_batches = []
    label_batches = []
    for i in range(1, 6):
        data = _unpickle('data_batch_%s' % (i, ))
        labels = np.array(data[b'labels'])
        data = data[b'data']
        data = np.reshape(data, (len(data), 32, 32, 3, ))

    for j in range(0, 100):
        data_batches.append(data[j:j+100])
        label_batches.append(labels[j:j+100])

    test_data = _unpickle('test_batch')
    test_labels = test_data[b'labels']
    test_data = test_data[b'data']
    test_data = np.reshape(test_data, (len(test_data), 32, 32, 3, ))

    return data_batches, label_batches, test_data, test_labels

