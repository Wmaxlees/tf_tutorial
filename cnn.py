#!/usr/bin/python3

import numpy as np
import tensorflow as tf

from shared import data

# Get the data we will use for the tutorial
data, labels, test_data, test_labels = data.get_batches()

# HYPERPARAMETERS DEFINED HERE
number_of_epoches = 100

number_of_classes = 10

filters_1 = 15
kernel_size_1 = (4, 4, )
filters_2 = 12
kernel_size_2 = (3, 3, )

hidden_layer_size = 900

learning_rate = 1e-4

# Default Graph
with tf.Graph().as_default():

    # Placeholder for our inputs
    x = tf.placeholder(tf.float32, (None, 32, 32, 3, ), name='x')
    y = tf.placeholder(tf.uint8, (None, ), name='y')

    # Convolutional neural network
    b_1 = tf.Variable(tf.zeros(filters_1))
    conv_1 = tf.nn.bias_add(tf.layers.conv2d(x, filters_1, kernel_size_1), b_1)

    b_2 = tf.Variable(tf.zeros(filters_2))
    conv_2 = tf.nn.bias_add(tf.layers.conv2d(conv_1, filters_2, kernel_size_2), b_2)
    pool_2 = tf.nn.max_pool(conv_2, (1, 2, 2, 1, ), (1, 1, 1, 1, ), 'VALID')
    conv_out = tf.contrib.layers.flatten(pool_2)

    # Deep neural network
    hidden_layer = tf.contrib.layers.fully_connected(conv_out, hidden_layer_size)
    logits = tf.contrib.layers.fully_connected(hidden_layer, number_of_classes)

    # Convert class label to a vector
    y_one_hot = tf.one_hot(y, number_of_classes)

    # Loss operation
    xent = tf.losses.softmax_cross_entropy(y_one_hot, logits)

    # Training
    train_op = tf.train.AdagradOptimizer(learning_rate).minimize(xent)

    # Accuracy
    prediction = tf.cast(tf.argmax(logits, axis=1), tf.uint8)
    correct = tf.reduce_sum(tf.cast(tf.equal(y, prediction), tf.uint8))

    # Initialize the session
    with tf.Session() as sess:

        # Initialize all the variables
        sess.run(tf.global_variables_initializer())

        # Step through each epoch
        for epoch in range(number_of_epoches):
            # Step through each batch
            for batch_idx in range(len(data)):
                batch_data = data[batch_idx]
                batch_labels = labels[batch_idx]

                xent_value, _ = sess.run([xent, train_op], {x: batch_data, y: batch_labels})

                # print('XENT: %.4f' % (xent_value, ))

            correct_predictions = sess.run([correct], {x: test_data, y: test_labels})
            accuracy = (correct_predictions[0] / len(test_data)) * 100

            print('Epoch: %s' % (epoch, ))
            print('Accuracy: %.2f' % (accuracy, ))
