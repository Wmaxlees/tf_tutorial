#!/usr/bin/python3

import numpy as np
import tensorflow as tf

from shared import data

# Get the data we will use for the tutorial
data, labels, test_data, test_labels = data.get_batches()

# HYPERPARAMETERS DEFINED HERE
number_of_epoches = 1

number_of_classes = 10

# Default Graph
with tf.Graph().as_default():

    # Placeholder for our inputs
    x = tf.placeholder(tf.float32, (None, 32, 32, 3, ), name='x')
    y = tf.placeholder(tf.uint8, (None, ), name='y')

    # Initialize the session
    with tf.Session() as sess:

        # Initialize all the variables
        sess.run(tf.global_variables_initializer())

        # Step through each epoch
        for epoch in range(number_of_epoches):
            # Step through each batch
            for batch_idx in range(len(data)):
                batch_data = data[batch_idx]
                batch_labels = labels[batch_idx]

                sess.run([], {x: batch_data, y: batch_labels})
