#!/usr/bin/python3

import numpy as np
import tensorflow as tf

from shared import data

# Get the data we will use for the tutorial
data, labels, test_data, test_labels = data.get_batches()

# HYPERPARAMETERS DEFINED HERE
number_of_epoches = 100

number_of_classes = 10

hidden_layer_size = 900

learning_rate = 1e-7

# Default Graph
with tf.Graph().as_default():

    # Placeholder for our inputs
    x = tf.placeholder(tf.float32, (None, 32, 32, 3, ), name='x')
    y = tf.placeholder(tf.uint8, (None, ), name='y')

    # Deep neural network
    x_flat = tf.reshape(x, (-1, 3072, ))
    hidden_layer = tf.contrib.layers.fully_connected(x_flat, hidden_layer_size)
    logits = tf.contrib.layers.fully_connected(hidden_layer, number_of_classes)

    # Convert class label to a vector
    y_one_hot = tf.one_hot(y, number_of_classes)

    # Loss operation
    xent = tf.losses.softmax_cross_entropy(y_one_hot, logits)

    # Training
    train_op = tf.train.AdagradOptimizer(learning_rate).minimize(xent)

    # Accuracy
    prediction = tf.cast(tf.argmax(logits, axis=1), tf.uint8)
    correct = tf.reduce_sum(tf.cast(tf.equal(y, prediction), tf.uint8))

    # Initialize the session
    with tf.Session() as sess:

        # Initialize all the variables
        sess.run(tf.global_variables_initializer())

        # Step through each epoch
        for epoch in range(number_of_epoches):
            # Step through each batch
            for batch_idx in range(len(data)):
                batch_data = data[batch_idx]
                batch_labels = labels[batch_idx]

                xent_value, _ = sess.run([xent, train_op], {x: batch_data, y: batch_labels})

                # print('XENT: %.4f' % (xent_value, ))

            correct_predictions = sess.run([correct], {x: test_data, y: test_labels})
            accuracy = (correct_predictions[0] / len(test_data)) * 100

            print('Epoch: %s' % (epoch, ))
            print('Accuracy: %.2f' % (accuracy, ))
